var https  = require('https');
const { promises } = require('fs');

async function getUsernames(threshold) {

  const prom1= await prom(1,threshold);
  const prom2= await prom(2,threshold);
  //console.log(prom1,prom2);

  
 // return prom1.concat(prom2);
}

 const prom = async (i,threshold) => {
    var options = {
        host: `https://jsonmock.hackerrank.com/`,
        path: `api/article_users?page`
    };

    let url= `${options.host}${options.path}=${i}`;
     https.get(async (url,res)  => {
         res.setEncoding("utf8");
         let body = "";
         await res.on("data", data => {
             body += data;
         });
         await res.on("end", () => {
             body = JSON.parse(body);
             let newBody = body.data.filter(x => x.submission_count > threshold).
                 map(x => x.username);
             if (newBody.length > 0) {
                 console.log(newBody);
                 return (newBody);
             }
         });
     })   
}
getUsernames(10).then(x=>{
    // console.log(x);
     return x;
 });