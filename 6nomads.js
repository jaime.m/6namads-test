var https  = require('https');

async function getUsernames(threshold) {
  let names=[];
    let prom1= await prom(1,threshold);
    let prom2= await  prom(2,threshold);

    Promise.all([prom1,prom2]).then(values=>{
       console.log(values[0].concat(values[1]));
       
    })
}

const prom =(i,threshold) => {
    var options = {
        host: `https://jsonmock.hackerrank.com/`,
        path: `api/article_users?page`
  };
    let url= `${options.host}${options.path}=${i}`;

  return new Promise((resolve,reject)=>{
      https.get(url, res => {
          res.setEncoding("utf8");
          let body = "";
          res.on("data", data => {
              body += data;        
          });
          res.on("end", () => {
              body = JSON.parse(body);
              let newBody= body.data.filter(x=>x.submission_count>threshold).
              map(x=>x.username);
              if(newBody.length>0){
                  resolve(newBody);
              }
          });
      })   
  }) 
}
